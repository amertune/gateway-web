---
layout: gw
permalink: /people/players/
title: Players
---
## More to Come

> I am currently in the middle of rebuilding GateWay's website, and the section you are looking at has not yet been completed. As soon as it is ready, we will be replacing this page with the information you seek.
>
> In the meantime, feel free to contact me if you have any questions. The best way to reach me is to log in to the mud and mail me, but e-mail will work as well.
> <cite>&mdash;Darkman</cite>
