---
layout: gw
permalink: /howtoplay/new_players/
menutitle: New Players
title: A Guide for New Players
---
## A New Player's Guide to GateWay

> The following guide was originally penned by Silverfrost. I have included it here with only very minor revisions.
> <cite>&mdash;Darkman</cite>

[The Most Basic](#basic) (I have never MUDded before)    
[Communication](#communication) (How to talk to us)    
[Emotes and semotes](#emotes) (How to perform actions)    
[About yourself](#yourself) (Knowing and developing your character)    
[Equipment](#equipment) (How to arm yourself)    
[Monsters](#monsters) (Where to find them and what to do when you do)    
[Gathering Items](#gathering) (How to get stuff)    
[Selling Items](#selling) (Where to do it and how the shops work)    
[Levels and Leveling](#leveling) (How to get better and stronger)    


## <a name="basic"></a>The Most Basic (I have never MUDded before)

MUDs are text-based role-playing games. There are no graphics, so everything is done through syntax and description. In a way, this does make them fairly easy to learn, but it can also make them more difficult. You will control the actions of your character by entering basic commands into the game, at the prompt. The prompt looks like this:

~~~ ansiterm
>
~~~

So whenever you see that symbol in further examples, remember it is the default game prompt, not a character you actually need to type. Anytime you see a word inside < > the means its a fill in the blank... so look <your name> would mean you would have to type look, followed by whatever character name you chose.

One of the best things you can do is to read through some of the help files. To do this, simply type help at the command prompt:

~~~ ansiterm
> help
~~~

This will give you a list of different help topics, and you can read the help files by typing help <topic>:

~~~ ansiterm
> help newbiecommands
~~~

To see the room you are in, use the look command:

~~~ ansiterm
> look
~~~

You can also use this to look at things... like a sign:

~~~ ansiterm
> look at sign
~~~

The command examine (or exa for short) i works pretty much the same way as 'look at'. To get a list of players who are currently playing the game you can type who:

~~~ ansiterm
> who
~~~

This will show you who is currently on and active. You will learn about communicating with these people later on. When you first create a new character, you will start in the newbie reception room. In this room there are several informative scrolls you may want to read.

~~~ ansiterm
> read <scroll name>
~~~

They will help you with some more of the basics I have not covered here. If for some crazy reason you think you want to leave GateWay, all you have to do at anytime is type quit.

~~~ ansiterm
> quit
A shimmering portal appears and you step back into reality.
As you leave, you hear a voice say: ~Come back soon, friend.~
Saving CharName.
~~~

Now, since no one ever wants to quit GateWay, we can move on to what you do in order to move around. I have already told you, you will start at the Newbie Reception Area. There are two exits out of this room... north and south. The game handles abbreviations for the basic directions, so you only need to type n or s to go north or south:

* n - north
* s - south
* e - east
* w - west
* ne - northeast
* se - southeast
* nw - northwest
* sw - southwest
* u - up
* d - down

In this case, either direction will take you to the Newbie Stand. The Newbie Stand is the center of the GateWay universe. There are generally several people hanging around and lots of people passing through, so a confused newbie never has to wait long to grab some help. The Newbie Stand also give out some neat stuff to newbies! Once you arrive there, all you have to do is type newbie and see what happens!

~~~ ansiterm
> newbie
The attendant smiles and hands you a short sword and a brown cloak.
Fare ye well, Adventurer.
The attendant also hands you a scroll.
~~~

Newbie equipment! So long as you are a newbie, you do not have to worry so much about equipping yourself. Now I think it is safe to move on to our next chapter... how do you talk to the rest of us?

## <a name="communication"></a>Communication (How to talk to us)

(say, tell, gossip, shout, whisper)

There are several commands used for communication. Here we will go over each and what the output will be. Next chapter you will learn how to perform actions.

say - The most basic talk command. This allows everyone who is in the same room with you to hear what you are saying. You can type say <message> or ' <message>. Either command will work:

~~~ ansiterm
> say hello
You say: hello

> ' hello
You say: hello
~~~

Either way the results are the same...

tell - This is a long distance communication. This allows you to talk to one person at a time who is not in the same room as you. The syntax is tell <player> <message>. If someone tells a message to you, you can also use the reply command, but this only works for the last person who has sent you a tell.

~~~ ansiterm
> tell silverfrost hello
You tell Silverfrost: hello

[33mSilverfrost tells you: hello[2;37;0m
>reply hello
You tell Silverfrost: hello
~~~

gossip - This is a mudwide chat line. When you first log in, you will get many welcomes and hellos over the gossip line. To respond to all the nice greeters, just use gossip <message>:

~~~ ansiterm
> gossip hello everyone
[35m[Gossip] NewBee: hello everyone[0m
~~~

shout - Similar to the gossip line, this can be heard mudwide. Since the creation of the gossip line, it doesn't get used very often anymore, though. To do this you type shout <message>:

~~~ ansiterm
> shout hello MUD
[31mYou shout: hello MUD[0m
~~~

whisper - This command will allow you to talk to another player who is in the same room as you without being heard by others. Rest assured, though, others will know you are whispering, even if they can't hear what's being said. To do this type whisper <player> <message>:

~~~ ansiterm
> whisper silverfrost hello, i am new..
You whisper to Silverfrost: hello, i am new..
~~~

I think that about covers it... You will run into more communication channels as you join guilds and clubs and find other aspects of the game, but these are the basics. :) Now, on to the next chapter...

## <a name="emotes"></a>Emotes and semotes (How to perform actions)

In addition to talking, you can interact with other players through use of emotes, or soul commands. There are dozens of them built in to the game, and you also have the ability to create your own.

semote - This commands used on its own will give you the list of soul commands that are built into the game. To do any of these commands you just type it on its own or with a player's name...

~~~ ansiterm
> smile
You smile.

> smile silverfrost
You smile at Silverfrost.
~~~

Because we have silly wizards, some of our soul commands are rather silly too, so if you want to know what a soul command will do before you use it, use the temote command... as in

~~~ ansiterm
> temote snirk
me:
You smirk.
others:
NewBee snirks. Man what a tool.
~~~

This gives you the output that you see, and what others see, without actually reporting it to the room.

emote - You also have the ability to make up your own soul commands. In order to do this, you need to type it out as you want the reader to see it, and you use either the emote command or use the character :

~~~ ansiterm
>emote smiles like a fool.
->NewBee smiles like a fool.


> : smiles like a fool.
NewBee smiles like a fool.
~~~

If you were to do >emote smile like a fool It would read NewBee smile like a fool. So, pretending emote or : replaces your name, you write out the emote exactly as you would want it to be read. This also works on chat lines, like the gossip line.

~~~ ansiterm
> gossip :grins.
[35m[Gossip] NewBee grins.[0m
~~~

So, that is the basics of emoting. On to the next chapter...

## <a name="yourself"></a>About yourself (Knowing and developing your character)

Gateway is basically a role-playing game. Role-playing is encouraged, but not strictly enforced. Have fun with it, but don't feel pressured. :) You have already defined some aspects of your character... you know your name, you know your race. You now have several options on how to further develop yourself.

description - You have the ability to set a description for yourself. All new characters have a default description that is defined by their race. You can make yours whatever you want it to be. The command is description. This will put you into the description editor where you can let creativity take over. To exit the editor, use **

~~~ ansiterm
> description Set thou description, Adventurer.....
[ Input ** to end or ~q to abort ]
]Before you you see a mighty warrior, dressed head to toe in all black, with long black hair.
]He has many battle scars and you can tell
]that each one must have a story behind it.
]Despite his massive appearance, he still looks at you with warm eyes and smiles.
]**
~~~

Joining a guild - Another way to develop who your character is, is to join a guild. There are many guilds to choose from, and each has its own personality. See the [Guilds](/guilds) page for more information.

role-playing - Again, there are no hard and fast rules on role-playing in GateWay, but you are most welcome to. Unlike role-playing video games, no aspect of your character is set in stone. You can be whomever or whatever you wish.

meet the player - We now also have a 'Meet The Player' section on our website. You are welcome to submit to me a character description, a story about your character, a picture(character), or anything else you would like to include. A cautionary note, though... because this is a public website, all character descriptions will be subject to the approval and/or editing of the admin. See also the Creating a Character Page. Email and questions or requests to: Silverfrost The person behind the player Maybe you would like to us to get to know the real you... Thats fine too! There are also several methods available to accomplish this...

chfn - The chfn command is used to modify the finger information you have available. You can enter your real name, if you so choose, or any other name, and also an email address where you can be reached if necessary. The email address is only visible to the admin staff. The personal info room - In the game itself, there is a room available where you can enter all kinds of personal information. You can skip any of the fields you don't want to answer, or answer them all, its up to you! Directions from the newbie stand are: s, s, e, s, ne, d - once in that room, type help info for a list of commands to use while in there.

Personal pages - If you so choose, you may also have a personal page in addition to your character page. Because we are trying to encourage character development, I must ask that you do not submit a personal page unless you have submitted a character page. We want to know the character first, then the person. For the personal page, you can give us a brief bio, a pic, family info, and a link to a homepage if you have one. If you question any info you want to add, feel free to email. I try to be pretty quick about it. :) Email and questions or requests to: Silverfrost So now we know some more about you... and you can know some more about us by visiting the People page. Let's move on to the next chapter, shall we?

## <a name="equipment"></a>Equipment (How to arm yourself)

Ok... I have already told you where to get the basic newbie equipment, but what about better stuff? How do you equip yourself? Let's first go over some basic equipping commands... Once you have equipment, you want to be able to use it, right? First, to see what you are carrying, you can type i

~~~ ansiterm
> i
You are carrying the following possessions:
  Newbie scroll.
  short sword.
  brown cloak.
~~~
    
This will show you your inventory, which as a newbie, won't be much, but it is an invaluable command. There are several commands that allow you to use the equipment that is in your inventory. The most basic is equip. This will equip whatever you have in you inventory in order.. i.e. if you have two swords, the first sword will be the one equipped.

~~~ ansiterm
> equip You prepare yourself for battle.
 - Wielding your shortsword: Ok.
 - Wearing your brown cloak: Ok.
Ok.
~~~

Now, when you check you inventory, it will show you what you are equipped with...

~~~ ansiterm
> i
You are carrying the following possessions:
  Newbie scroll.
  short sword (wielded).
  brown cloak (worn).
~~~

If you wish to remove the equipment you are wearing, unequip will take care of that for you...

~~~ ansiterm
> unequip
You give up on the futility of it all and get NAKED!
 - Unwielding your shortsword: Ok.
 - Removing your brown cloak: Ok.
~~~
    
Another way to equip, especially when you have more then one sword or several pieces of armor, is to use the commands wear and wield. Wear is for armor and wield is for weapons...

~~~ ansiterm
> wear armor
Ok.
> wield weapon
Ok.
~~~

To remove equipment, you can still use unequip, or you can use remove and unwield.

~~~ ansiterm
> remove armor
Ok.
> unwield weapon
Ok.
~~~

Every item in your inventory has an alternate name and a specific purpose... To see an example of this use the eq command.

~~~ ansiterm
> eq
[Battle Equipment Status]

     Wielded : short sword (wielded)

     Helmet  : <none>
     Amulet  : <none>
     Cloak   : brown cloak (worn)
     Armor   : <none>
     Ring    : <none>
     Gloves  : <none>
     Shield  : <none>
     Belt    : <none>
     Boots   : <none>
     
     Armor Status: very lightly armored
     
     Eyewear : <none>
     Earring : <none>
     Outerwea: <none>
     Wearable: <none>

     Light sources: <none>
~~~
    
As you can see, not only can your equipment be referred to by generic names, such as weapon and armor, but also by more specific names, such as cloak and sword. This will come in handy when you start collecting more equipment... using the term armor will only grab the first piece of armor in your inventory... if you are carrying gloves, boots, and a cloak, and you want to wear your cloak, wear armor may very well equip your gloves instead... so wear cloak would work best here. As an aside, if you are carrying more than one cloak, you will need to refer to them by number or by a more specific name, which can usually be gotten by looking at an item... i.e. wear cloak 1, wear cloak 2; wield longsword, wield shortsword, etc. Now we have learned some of the basic equipping commands... how do you get more stuff?!

Contar - Contar is a newbie helper. He will be your best friend until you graduate newbie-hood. Located 4 n, 2 e, n of the newbie stand he not only has a chest of equipment that you can borrow from, but he also will take you to any number of newbie areas where you can begin killing. To equip yourself from Contar's chest, start by looking to see what's in there. If you are the first newbie to have been at the chest today, it may be closed. In this case, just open chest. Inside the chest there will be some basic equipment. You can use any of this equipment by getting it from the chest.

~~~ ansiterm
> get sword from chest
You get the Std issue sword from the Contar's war chest (opened).
~~~
    
Here is where learning to differentiate between swords comes into play.. Our imaginary player Newbee now has a Newbie Sword and a Std issue sword. Looking at your inventory, you now have two swords.

~~~ ansiterm
> i
You are carrying the following possessions:
  Std issue sword.
  Newbie scroll.
  short sword (wielded).
  brown cloak (worn).
~~~

If you look at each sword, you see they are different types.

~~~ ansiterm
> look sword
Its a simple standard issue guard sword.
This weapon appears to be a longsword.
> look sword 2
This simple shortsword is designed for beginners in swordplay. The blade is a lightweight silver so as not to wear out the arm of the user. The handle is made of durable, yet elegantly carved wood. Looking closer, the carvings are of ancient runes. The sword feels light and strong. You should probably "equip" so that the sword is ready to fight.
This weapon appears to be a shortsword.
~~~

In order to wield the Std sword, you would type wield longsword, or even just wield sword because it is the first sword in your inventory; but... to wield the Newbie sword, you would need to type wield shortsword. Now we have learned the basics of equipping... You will be able to find more and better equipment as you travel around the realms and kill mosters... Now, where do you find those monster?


## <a name="monsters"></a>Monsters (Where to find them and what to do when you do)

Ok.. so now you are equipped and pretty comfortable with the whole moving around thing. Now its time to start killing. There are many places a newbie can travel to in order to find creatures to kill. Since we last left you in Contar's Hut, let's start there.

Contar - Contar is extremely helpful to the newbie adventurers of GateWay. Not only will he equip you, but he will take you to many fascinating places where you can begin your adventures. All you have to do is ask him to tell you a story.

~~~ ansiterm
>' story
You say: story

Contar scratches his head. His voice takes on an almost hypnotic quality.
Contar wheezes: Let me tell you of a place I know...
~~~
    
Footpad Island - The Island of Footpad in the Domain of Shadow is a merry land of hills and trees. It's also the home of the Little Critter Club. I'm not sure if they are accepting applications for new members... perhaps the guard there will know more about it!

The Newbie Funhouse - The Newbie Funhouse is one of the rooms inside the Hotel Fantasia in UnderGround. You will find a variety of clowns, children, and vendors, as well as a shady Ticket Man. However, they say that Bonzo the Evil Clown has set up shop there; perhaps you can put him in his place.

The Gibberlings - Many years ago, the capital city of Callis was engulfed in lava from the volcano on the island. The city was rebuilt upon its own ruins, but a number of cracks and crevices were left in the foundations of the city. The Gibberling King has taken up residence in one of these subterranean halls with his minions.

Celtic Training Grounds - The lands of the Celtics in the domain of Callis provide many challenges for the young adventurer. You will start at a mysterious clearing with stone pillars. Look carefully and you will see which way is best for you. Newbie Academy of the Orient - The Orient Newbie Academy is an excellent place to visit if you have never been in a place like GateWay before. It has a series of rooms that will introduce you to basic commands used here. Once you have mastered the lessons you learn there, you may graduate to the Fields of Inari to test your skills.

The Orient Rainforests - A tropical rainforest lies in the Domain of Orient. Journey high above the treetops to discover local plant and animal life. Listen to the chatty parrots as they have much to say about their habitat. Rumor has it that there is a treetop village, but only the brave may find it.

Crystal Mansion - An old mansion stands on a hill in the domain of Crystal. Although no one has lived there for years, there are rumours of strange noises being heard from within. Surely there are some skeletons in closets to be found there!

Jarrod's Garden - Jarrod's Garden in UnderGround is full of worms, ants, bees, and other fanciful inhabitants. Try not to disturb Jarrod if you see him, and above all, keep yer mitts off his vegetables! Caution! This area is a bit more difficult than some of the others. You might want to tackle it only if you are nearing a 30% ability rating or so.

Baron Von Gourd's - Years ago, Baron Gord donated a portion of his land to provide training grounds for those new to the domains of GateWay. Many novice adventurers sought out this area, and enjoyed the challenges that it provided. Alas, unbeknownst to the Baron, a deep-rooted evil presence lay buried in the fertile soil.

Note: Do hang onto the rabbit's foot that Contar gives you. If you go back into the room where he first dropped you off and squeeze the foot, he will take you back to his hut. I left Baron Von Gourd's for last, because this is one of the easiest to get to on your own, without Contar. It is important that you start exploring early on, because once you graduate newbiehood, Contar will not be willing to drag you around any longer. So, to get to Von Gourd's on your own, you can travel 4 e, 3 ne, e from the Newbie Stand. Jarrod's Garden is another simple one, when you get a little bit stronger.. travel east from the Newbie Stand until you find it. Now that you have found creatures, how do you kill them? Well, that's more straight-forward than it seems. Simply use the kill command...

~~~ ansiterm
> kill
~~~

This will begin the battle. Once in battle there are several commands that you can use. The sc command will check your hit points.

~~~ ansiterm
> sc
[HP: 46/(52) SP: 52/(52)]
~~~

The shape command will check the condition of the monster.

~~~ ansiterm
> shape
** AN ELVEN SKELETON IS MODERATELY INJURED. **
~~~

Now, I know this will never happen, but let's assume for a moment that you got in over your head and your hit points are getting low. All you have to do to escape a battle is leave the room. All newbies start out with a 10% wimpy. This will force you to run from a battle when you are at 10% of your available hit points, but that is only about 6 hp. You can set your wimpy up to 20% without any penalty. Anything over 20% will cause you to lose experience when you flee. Use the wimpy command.

~~~ ansiterm
> wimpy 20
You now wimpy at 20%.
~~~

Use caution, though. The wimpy command by itself will put you into brave mode, which means you will not flee battles.


## <a name="gathering"></a>Gathering Items (How to get stuff)

Now that you have begun killing creatures, you can begin to collect more equipment and other items. Equipment and special items can be acquired many ways. Looting corpses, search rooms, completing quests... Quests may come a bit down the line, though...

Looting Corpses - When you kill something, it is not uncommon for there to be anything from weapons to money on the corpse of the creature. In order to find out, you want to use the get command.

~~~ ansiterm
> get all from corpse
~~~

This will pull any loot off from the corpse and put it into your inventory.

Searching Rooms - Equipment and items are not always found on corpses. Sometimes they can be discovered right in the room you are in! For example, using Baron Von Gourd's newbie area, I will show you a typical room:

~~~ ansiterm
> l
[33m[Southern edge of a desolate wasteland][0m
Standing along the edge a continuous entanglement of thorny brambles, your sight is filled with the aftermath of an ancient battle. The soil is barren and littered with useless armor and rusting weapons. A frigid wind stirs clouds of dust that stick to your sweaty skin. Although the entanglement blocks any passage to the south, the remainder of the wasteland lies open for adventure.
    [31mThere are three obvious exits: north, west and east.[0m
A halfling skeleton.
~~~

In order to search a room, you use the search command. Let's try it in this room, shall we?

~~~ ansiterm
> search
You are too nervous to search with an undead monster so close by.
~~~

Ahh... so we must kill the skeleton first. Ok, then! ............ There, now he's dead... let's try that search again.

~~~ ansiterm
> search
Perhaps you should specify what you want to search.
~~~

Well, what the heck? Ok... let's take another look at that room description. What could we possibly search in here? The soil?

~~~ ansiterm
> search soil You search, but fail to find anything of interest.
~~~

Ok... the armor? The weapons?

~~~ ansiterm
> search armor
You dislodge something from the arid soil.
~~~

Alright! Use the get command again to retrieve your discovery.

~~~ ansiterm
> get all
You pick up: a rusty shield
~~~

Woo hoo! Now to equip or wear it, and we are set to continue adventuring!!

~~~ ansiterm
> equip
You prepare yourself for battle.
 - Wearing your a rusty shield: Ok.
Ok.
~~~

Now you know how to loot your corpses and search out items. Use caution, now. Newbies are of low strength and can only carry so much loot. We will learn how to sell in the next chapter.


## <a name="selling"></a>Selling Items (Where to do it and how the shops work)

Selling items will become a necessity, because Newbies are a little weak to carry too much stuff. Every realm has at least main shop and several minor shops. The easiest one for a newbie to get used to is the shop in Chaomonous, because it is so close to everything else. To get to the shop travel east then south from the Newbie Stand. The Shop - Here is where you can sell and items you do not want... There are several ways to sell only what you want and not things you want to keep. If you use the inventory i command, your items will be numbered. You can sell by number, i.e. sell 1, sell 3, sell 6... make sure you check i after each sell though, because numbers will change. If you have equipped the only items you want to keep, then you can just type sell all and it will sell everything in your inventory you are not wearing/wielding. One final way to differentiate between what you do and don't want to sell is to use the tag command. Tagging items will stop them from being sold or dropped unless you untag them. You can tag or tag all, and to remove the tags, untag or untag all.

~~~ ansiterm
> tag all
You tag all of the valuables in your inventory.
> untag all
You untag all of your inventory.
~~~

Not only can you sell, but also buy from these shops. To see what is in the shop, you can use the list command. Depending on what shop you're in, this list can be rather long, so you can also list by specific types of items... i.e. list sword, list armor, list gloves, etc. The items in the shop will then be numbered, so you buy by number... buy sword 1, buy 1, buy armor 10, etc. Now that you now how to sell your stuff, let's talk about advancement.


## <a name="leveling"></a>Levels and Leveling (How to get better and stronger)

Advancing on this game is rather self-explanatory... You kill things, you advance. You will see advancement messages when you have gained another level. Unlike a lot of MUDs, GateWay's leveling system is based on percent, not exact experience. You will never know exactly how much experience you have gained. You will only know what percent you're at. Let's take a look at some commands that deal with your percent and stats. Score - To take a look at your basic stats, use the score command. This will give you a screen that shows you your ability rating, your attributes, your hit points, your spell points, and several other useful pieces of information. Let's take a look at an example score screen:

~~~ ansiterm
> score
+---------------------------------------------------------------------------+
                         Newbee the wandering freeman (neutral)
                       Guild: None           Race: Human

           Battle:   62 (62) hit points        64 (64) stamina points
           
                STR: 10/110      INT: 7/100      WIS: 7/100
                DEX: 6/ 90       CON: 6/100      CHR: 6/100
                
                        Overall Ability Rating: 7 %
           'You are midway in reaching the next ability level'
            [========================>.....................]
            
Money:  253 drachmas  201 rilks  419 royals  414 tarins  214 yeuns
Quests: You have earned an average bonus.
Mentor: You are receiving the minimum bonus.
Status: You are presently fully awake.
Armour: You are unarmored.
Weight: You are unencumbered.
Health: You are getting a bit hungry.
Attack: Wimpy at 20%
Age:    3 hours 8 seconds.
+---------------------------------------------------------------------------+
~~~

Stats - Another useful command that will show you how you are doing when it comes to killing things is the stats command. This screen will show you information like how many kills you have had, how may unique kills, how much money you have made, what you have killed and when, how many times you have died and to whom, etc.

~~~ ansiterm
>stats
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

General Info
-----------------------------------------------------------------------------
Total Kills: 4 kills
Total Unique Kills: 2 kills
Kill Average: 17%
Kill Rate: You've averaged 4 kills each day of your life (0.1 days).
           You've averaged 4 kills each day since Aug 2002 (0.1 days).
Highest Session Kills: 4 kills on Thu Aug 22, 2002
Highest Session Kill Rate: 4 kills per hour on Thu Aug 22, 2002

Total Deaths: 1 deaths

Highest Session Advancement: Gained 6% from 1% to 7% on Thu Aug 22, 2002
Highest Session Exp Gain: Occurred going from 1% to 7% on Thu Aug 22, 2002
Highest Session Exp Rate: Occurred going from 1% to 7% on Thu Aug 22, 2002

Total Money Gained: 3002 coins
Total Money Lost: 1501 coins
Net Profit: 1501 coins
Highest Session Profit: 1501 coins on Thu Aug 22, 2002

Your character was created on Thu Aug 22, 12:20:12 2002.
You've spent 100% of your time logged into GateWay.
-----------------------------------------------------------------------------

Kill Log:

Monster                           Your Percent        First Killed On
-------                           ------------        ---------------
Ghoul                                    1%            Thu Aug 22, 2002
Skeleton (3)                             5%            Thu Aug 22, 2002
-----------------------------------------------------------------------------

Death Log:
Monster                         Your Percent         You Died On
-------                         ------------         -----------
Wight                                   7%         Thu Aug 22, 2002
-----------------------------------------------------------------------------
=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
~~~

Toplists/playerrank - If you are one of those highly competitive newbies, there are several ways of checking how you rank against other players. You can use the toplist command, which by itself will show the top 15 ranked players in GateWay. You can also use it with a number and it will show you the next 15 ranked players starting with that number... i.e. toplist 16 will show you those ranked 16th through 30th. There are also several versions of toplist... using help toplist will give you the list, you you can check the top ranked players per guild, race, profit, kills, etc. Another command you can use, which might be slightly handier has a newbie, is playerrank. In this case you playerrank and it will tell you how ranks among other players on GateWay. Most of the same versions of toplist work with playerrank as well... i.e. playerrank profit, playerrank kills, etc. Some, however, do not, such as guild and race, while playerrank has some commands that do not work with toplist, such as playerrank deaths...

~~~ ansiterm
> playerrank newbee
Newbee is ranked 123 among active players on GateWay.
> playerrank newbee deaths
Newbee is ranked 107 among active players on the deaths list.
~~~

You have now been taught much valuable information about being a new adventurer in the Realms of GateWay. To learn more, you can visit the Overview page, the Guilds page, the Reference page, or many of the other pages in this site. You also can learn by playing the game and talking to any number of friendly, helpful characters that call GateWay home. Good luck, Adventurer!
