---
layout: gw
permalink: /places/newbiestand/
title: Newbiestand
---
## The Newbie Stand
> I am currently in the middle of rebuilding GateWay's website, and the section you are looking at has not yet been completed. As soon as it is ready, we will be replacing this page with the information you seek.
>
> In the meantime, feel free to contact me if you have any questions. The best way to reach me is to log in to the mud and mail me, but e-mail will work as well.
> <cite>&mdash;Darkman</cite>

<img class="img-rounded pull-right" style="margin-right: 10px;" src="/img/cezanne_pyramidofskulls.jpg"> The Newbie Stand is a great place to idle, chat, drop off gear, pick up gear, or leave skulls. Especially the skulls, we love skulls.

<!-- clearfix is a hack to reset the flow after floating the image -->
<div class="clearfix"></div>

Paragraph 2, underneath the image
