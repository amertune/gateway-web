---
layout: gw
permalink: /lore/history/
menutitle: History
title: A Brief History of Gateway
---
## A Brief History of GateWay

> What follows here is, I believe, a reasonably accurate account of some portion of the major history of GateWay as a MUD. It is certainly incomplete (we did once try to create a full and complete listing of all the sites which had hosted GateWay throughout the turbulent years of the 1990s, but in the end we were unsuccessful), but it does give an idea of the number of people who have been involved over the years.
>
> As of this writing (16 January, 2005), there are 143 wizard characters in existence who have lent their hand to creation or maintenance of some aspect or another of the game. Many of those people have long since gone on to other things, and there are some others still who have removed their characters (or been removed, in a select few more colourful cases) but their contributions remain a part of the international creative collaboration that is GateWay.
> <cite>&mdash;Darkman</cite>

## In the Beginning

In January 1989, a new LPMUD called "Underground" opened to the public under the leadership of Pelmen. It was based in Toronto, Ontario and was the first open LPMUD in Canada. It thrived for over a year, growing into a reasonably sized populace of players and wizards. Unfortunately, as is ultimately the fate of most MUDS, Underground lost its site in February 1990.

At this same time, a new mud was under development at Dalhousie University, in Halifax, Nova Scotia. The project, called "Haven", was being lead by the redoubtable Badaxe.

Underground, at the time of its unfortunate demise, was under the leadership of a wizard named Watcher. Watcher also attended Dalhousie University, and was a friend of the redoubtable Badaxe. The two were mournful of the potential loss of the rich and detailed realms present on Underground, and decided to combine the two separate muds into a new project they named "GateWay".

The GateWay project was initiated in April 1990, with Watcher being the first character to ever logon (there's a piece of GateWay trivia for you). After a few months of wizard-only development to integrate the two areas (now termed domains or realms), GateWay was opened to the public. Badaxe became GateWay's first GateKeeper, with Watcher the chief Elder (another Underground wizard named Falchan was the other Elder). After about six months of leadership, Badaxe handed over the GateKeeper mantle to Watcher and stepped back into a behind-the-scenes role, in charge of the site and game driver maintenance.

GateWay continued to grow over the next year, adding the new domains of Callis, Crystal, and ultimately Shadow (in that order). At this time, the domain of Orient was also under development, though its growth was slow and would not open to the public for a few years to come.

In August 1991, Watcher formed a new Elder Council composed of experienced GateWay wizards to lead the mud into the future. After ensuring the transition was smooth and complete, Watcher retired and left GateWay for a time. GateWay continued under the leadership of the Elder Council for a number of months, but ultimately was forced to leave its home at Dalhousie University. It then wandered, for a time, throughout the internet, looking for a stable site to ultimately call home. From Halifax to Texas, to California to New York - GateWay has always found a home.

## The Dynasties

In the beginning of the Dark Ages of GateWay (basically the period after the great Watcher's first (of many) retirements to the present), there was no single leader. Instead, a number of elder wizards, theoretically equal in power, presided over the happy inhabitants of GateWay. The gates to the realms moved from bad machines to bad machine until a player named Ivan offered a computer named "idiot" to the elders, and the lowly inhabitants of GateWay were happy.

After his transformation into an immortal, Ivan slowly begin to take an active hand in the shaping of GateWay's destiny. The elders eventually took heed and tried to stop him, but alas, Ivan's control over the idiot proved to be too great an advantage, and many elders were slain in the ensuing struggle.

When the dust settled, Ivan had set him up as the Creator of GateWay, which was basically your garden variety head admin with a fancy name. Being a humble Christian, Ivan soon changed the title to Ancient, and he set up three elder wizards under him: Caryopsis, Falk, and Skywise.

Ivan, being skilled in the invention of titles and delegation of duties, gave each elder a business: Law, Balance, and MUDLib. All was smooth as Falk, the only active admin, basically ran the MUD.

When Falk retired, Ivan realized that the MUD had nobody to do admin work anymore, so Ivan tempted Caryopsis into becoming the next Ancient. It revitalized Caryopsis's interest in the MUD, and he ran the MUD smoothly for a long time.

Anyway, Caryopsis eventually grew tired of the hype and retired, and the wizard Ivor took over as Ancient, who quickly filled his elder slot with wizards like Ubu and Spock. There was a general feeling that the MUD was losing its "seriousness." Tanin and Skywise were both determined to become the next admin to restore the MUD to its old self.

It didn't take long before Ivor grew tired of telling people what to do, and he named Tanin, who was far more active than Skywise, the next despot of GateWay. Ubu said he was very sad at the turn of events, as he only agreed to become an elder because he thought the MUD would be a relaxed place to work in with Ivor leading the way. Ubu retired shortly after, and Tanin quickly filled the elder spots with more serious people.

The MUD wandered aimlessly for a while, until Ivan once again offered to host GateWay on one of his machines, this time with Noluck as the head admin. Noluck started using the title of GateKeeper.

In time, the wheels of power turned, and it came to pass that Darkman became GateKeeper, with Hawkwing, Nate, and Rumble presiding as a council of Elders. It was during this time that GateWay experienced a rebirth, and the player base grew once more from levels that had grown low. For reasons not widely understood, there was later a change in the Elder Council as Nate and Rumble chose to leave its ranks. Cougar was appointed to fill the open responsibilities, and Zor was also given the title of Elder. The Elder Council continued in this form for a year or so.

After long service, Darkman chose to cede the burden of the GateKeeper position, and by general acclaim, Watcher once again took it up. Watcher appointed Cougar, Darkman, Nudiebar, and Rumble to advise him as his Elder Council. After 11 cycles of the moon, it became necessary for another shift in power. Cougar and Watcher had already fallen to the perils of "Real Life" Watcher beginning a new life with more letters after his name, and Cougar, fighting for, and protecting good. Rumble also made this the time of his retirement, fading into the night; the fruits of his labor casting their seeds across all the realms. Nudiebar accepted the position of GateKeeper, appointing the wizards Numor and Maarken as his Elders.

Under the guidance of Nudiebar, GateWay flourished and Galgo was added to the Elder Council to balance the load on Numor and Maarken. Real life paid a visit to the GateKeeper and his staff, one by one, they resigned to their fate and obligations to the outside world and stepped down from the day to day activities of the Realms, but continued to work on "big picture" items for the Realms.

Darkman lost to Cougar in a coin toss, and was forced to reclaim the mantle of GateKeeper. Darkman filled his staff with two familar faces: Cougar and Hawkwing. Ghosty, former Lord of Crystal and the Orient, was also added to the Council. In time, Real Life took its toll on Cougar once again, and he removed himself from active duty as an Elder. Zor returned to the Council to take his place.

This is how it was; and how it is. What the future holds, we do not know. But we do know GateWay will exist, always...
