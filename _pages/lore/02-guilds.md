---
layout: gw
permalink: /lore/guilds/
title: Guilds and Powers
---
## The Guilds of GateWay

You will enter GateWay as an unskilled adventurer. GateWay has a number of enticing professions, known as guilds,
which offer their members various abilities to master the challenges in the realms. Early in your career, you will
want to join one of these guilds in order to take advantage of these abilities. Type "help guilds" to learn about these
professions, and don't hesitate to ask players to help you with joining them.

<img class="pull-left" src="/img/feather.png" alt="Feather by Lorc, CC-BY-3.0, game-icons.net">

## <a name="priests"></a>Priests of Cianna &mdash; Good Healers
The Priests of Cianna are a deeply religious group who serve the Goddess Cianna Lightbringer.  They work
to spread light and banish darkness throughout all of GateWay. Though priests are not known for their
direct fighting abilities, their healing and ability to withstand much damage is renowned throughout GateWay.

Priests do not receive any particular bonuses in hand to hand combat, as they tend to concentrate on healing
and withstanding punishment. However, they have the impressive ability to cast the stoneskin spell on
themselves, which allows them to withstand nearly all damage for a short time.  Of course, Priests are noted
for their potent healing spells which may be used on themselves or on other players.  Priests may also
cure diseases and other ailments of adventurers.

Members of any race may spread Cianna's light through the realms. A priest must maintain a good alignment, as
evil acts would nourish darkness which counteracts Cianna's light. Therefore, attacking opponents of good
alignment will cause your favour with the Goddess to slip. Cianna disapproves of all weapons that draw blood,
such as swords or axes. However, a Priest may wear any type of armour, the better to protect themselves from
the forces of darkness.

<img class="pull-left" src="/img/daemon-skull.png" alt="Daemon Skull by Lorc, CC-BY-3.0, game-icons.net">

## <a name="dws"></a>DevilWorshippers
During the dark times of the demon invasions and orcish raids, long ago, some of the population of
Haven City began to dabble in the dark arts of demonology and necromancy.  After many years, these
men and women banded together to form a guild most dark&mdash;that of the Devil Worshippers, a small,
elite group of extremely dangerous sorcerers. Many adventurers have tried to succeed in the dark arts,
but many fail. Only the smartest and most talented among us can overcome the difficulties and
excel in the black arts.

DevilWorshippers are masters of the dark arts and mind control. They can cast the dreaded Invoke Death
spell, and even mindlink their opponent in an attempt to gain control over them. DevilWorshippers can
also summon a host of imps and demons to torment their opponents and do their bidding. DevilWorshippers
can also invoke magical protection upon themselves and their allies.

A member of any race may be a DevilWorshipper.  However, the power of their spells is significantly
influenced by their intelligence, so it is rare to see the less intellectual races, such as Ogres, seeking the
training of this guild. Obviously, DevilWorshippers must maintain the most demonic of alignments. Since they
rely mostly on their powerful spells, DevilWorshippers wield only daggers, knives, and staves, considering all
other weapons to be unworthy and impractical. DevilWorshippers also hate most types of armour, since it makes them
clumsy. They wear only boots, cloaks, rings and amulets.

<img class="pull-left" src="/img/heavy-thorny-triskelion.png" alt="Heavy Thorny Triskelion by Lorc, CC-BY-3.0, game-icons.net">

## <a name="druids"></a>Druids of Gwydion
Not much is known about the secretive society of Gwydion's Druids. These powerful defenders
of nature are known however to inhabit the forest regions of southern Underground, protecting
their sacred groves and the animals living therein.

It is not known how many Druids still exist or if the path of their guild has been forsaken to
be lost in the vaults of history. However, if you search long and hard enough, you may be able
to find one of their number and the animal spirits which they are known to often be accompanied
by. If you prove worthy, perhaps you may be accepted into their secret society and made one of
their dwindling number.

Druids are primarily spellcasters who derive their powers from Nature itself.  They can summon and
control many of Nature's creatures to assist them as their servants. Mauling bears, stinging
insects, and entangling vines are all servants of nature who will answer a Druid's bidding. It is
rumoured that Druids can even call upon the very elements of Nature, such as wind, stones, and
lightning, to wreak havoc upon those who would upset Nature's balance.

Members of any race may join the druids, for all living creatures are part of the natural order. 
Druids are dedicated to preserving balance, and therefore they must maintain an alignment as close to
neutral as possible.  Druids may wield a variety of weapons such as longswords, staves, scythes, and
hammers, and many armours such as chainmail, helmets, and shields. However, as spellcasters they
disdain purely warlike equipment such as two-handed swords or platemail.

<img class="pull-left" src="/img/visored-helm.png" alt="Visored Helm by Lorc, CC-BY-3.0, game-icons.net">

## <a name="knights"></a>Lost Knights of Callis
The Lost Knights are dedicated to upholding chivalry and honour within GateWay. Masters of physical
combat, they have sworn to use their martial skills for goodness.

The Lost Knights rely on the might and skill of their arms. Their most impressive ability is the art of
Deadly Attacks, which allows them to strike several blows to each opponents one. They are also skilled in
the use of weapons and armour, and are able to sharpen and adjust these in order to improve their
effectiveness. Lost Knights have the ability to protect their comrades, and selflessly intercept the blows
meant for others. In addition, the Knights boast an intricate system of Honours which award them credit for
specific acts such as slaying vicious dragons and other foul beasts.

Members of all races who are pure of heart may join the Lost Knights. The armoury of the Knights is extensive,
and they may wield any weapon, although they tend to prefer the larger variety such as two-handed swords,
bastardswords, and axes. These warriors may also equip themselves with any armours that they might obtain,
for might lies in invincible armours as surely as in stout blows.

<img class="pull-left" src="/img/meditation.png" alt="Meditation by Lorc, CC-BY-3.0, game-icons.net">

## <a name="monks"></a>Monks of Marik
The Monks of Marik are a militant monastic order. The Monks follow the teachings of Marik the Wise. Marik
taught that personal balance and mastery of one's body and mind can lead a person to their full potential.

Monks are primarily fighters who have forsaken the use of traditional weapons and armors. Instead the Monks
quest to make their mind and body one with each other. Once this mastery is achieved, the true power of the Monk
is realized. It is common to see a Monk strike a critical life-ending blow to their opponents. Monks also
practice controlling their "Chi" or life energy. With this control, a Monk is able to project some of his energy
into his opponent, thereby causing additional damage to the foe.

Any race is the realms of GateWay may choose to follow the teachings of Marik and in turn become a Monk. Searching
for balance is a personal choice, and due to this choice, there are no restrictions to alignment. Due to the beliefs
and training of a Monk, the use of any weapon besides the body is disallowed. Furthermore, the Monks may not wear
any armor, which may constrict their movements.

<img class="pull-left" src="/img/lightning-bow.png" alt="Lightning Bow by Lorc, CC-BY-3.0, game-icons.net">

## <a name="rangers"></a>Rangers
Rangers are fighters who excel in the arts of woodscraft and hunting. They are experts in skills derived
from nature itself: animals, plants, and the very elements of nature.

The mere utterance by a trained Ranger of a word of power can accomplish remarkable feats. A magical
light can be summoned from the amulet which every Ranger carries, which will lights his way in dark
places. Magical forces can be invoked to protect in battle against evil foes, enemies can be confused
and caused to attack each other, and even the very forces of nature can be harnessed, to attack any of
the Ranger's enemies. As well, a magical Heartbow may be picked up at any time, from the Rangers'
guildhouse. This bow can shoot down even the mightiest of foes, as it uses the actual soul of the Ranger
to strike his enemies.

Members of any race may seek the teachings of this guild. However, Rangers must remain of a neutral or
good alignment, or else they will find their guild powers failing them. As well, due to their very nature,
a Ranger may not wear any metallic armour, as it impedes them in the wilderness. Furthermore, certain
weapons must be forfeited, for in the wilds, nothing serves a Ranger better than his trusty Bow. Since the
skill required to use such a mighty weapon is very great, certain other weapons, such as swords, may not be
wielded.

<img class="pull-left" src="/img/snake-totem.png" alt="Snake Totem by Lorc, CC-BY-3.0, game-icons.net">

## <a name="serpents"></a>Society of Serpent Warriors
The Serpent Warriors are a group of evil fighters who declare a fierce loyalty to the Serpent Goddess, Yitskar.
They are a warlike band who often use treachery to achieve their ends.

Serpent Warriors trust to their prowess in hand-to-hand combat. Their most famed ability is the Florentine
technique which enables them to wield and fight with two swords, one in each hand. They can also summon
fearsome serpents from the underworld to fight at their sides. Serpent Warriors are also experts in the use
of poisons, and can spit poison from glands at their opponents.

Yitskar accepts members of any race, for all can be of service to her evil goals.  Obviously, her followers must
maintain an evil alignment or surely they will fall into her disfavour. Serpent Warriors may wield any type of
weapon, but they receive specialized training in the mastery of swords, and thus they are most often found with
this weapon in hand. Serpent Warriors may also wear any sort of armour, excepting when they are wielding a sword
in each hand. It is not possible to wear a shield in this situation.

<img class="pull-left" src="/img/slingshot.png" alt="Slingshot by Delapouite, CC-BY-3.0, game-icons.net">

## <a name="shortbreds"></a>ShortBred Clan
The ShortBred Clan is a guild of mischievious faerie folk, infamous for their ability to make the bigger folk look
foolish. Established by Gobrus ShortBred, the immortal patron of the ShortBred Clan, the guild wields the Power of
the Secrets of Faerie.

With their lithe grace and lightning-swift speed, ShortBred are deadly opponents in battle, for they can dodge the
clumsy blows of their enemies, and sometimes strike two blows to their foe's one! They can even conjure a set of
illusionary images of themselves to entirely confuse their hapless opponents. The ShortBred also command an array of
mischevious curses to cast on other players, and solemn indeed is the Bred who can resist playing these tricks on
the clumsy larger folk of GateWay from time to time.

Only the pure of heart and fey of nature can hope to master the Secrets of Faerie; thus only dwarves, halflings,
gnomes, pixies, goblins, and elves may join the Clan, and of those, only those who serve the forces of Light will
be accepted. The ShortBred may use any weapons or armour, but they naturally tend to prefer dexterous sorts of
weapons, such as shortsword, staff, dagger, spear, bow, rapier, and sling.

<img class="pull-left" src="/img/rune-sword.png" alt="Rune Sword by Lorc, CC-BY-3.0, game-icons.net">

## <a name="vikings"></a>Vikings
Born of warrior stock, and hardened in the icy Fjords, the Vikings are a hardy lot. Given their strength by
Odin and Thor, they will use any means necessary to achieve victory, and are not above dastardly deeds to
get what they want&mdash;gold and glory.

Vikings are mighty berserkers who can oft times ravage their foes with hammer and helmet alone.  In combat,
they can fly into a fearsome rage much to the dismay of their enemies. They have been bestowed with Odin's
mighty spear, Gugnir, and have a variety of runes which can be scribed upon weapons, armours, and other items
to improve their abilities. They are among the most physically strong on GateWay, and rely heavily on this.

Those not of strong stock need not apply, as Gnomes, Elves, and Pixies will not be considered for membership.
Vikings can be of any alignment as the gods condone treachery as well as sainthood so long as the rewards are
tempting.  Vikings are forbidden from using such delicate items as bows and staves and specialize instead in
more destructive weapons such as large swords, hammers, axes, and spears. Vikings can don any piece of armour,
though Odin is impressed by the barechested, shield-gnawing, hairy berserker.
