---
layout: gw
permalink: /lore/maps/
title: Maps
---
## Maps of GateWay

> A major aspect of playing GateWay is exploring. Many rooms have secret items or passages, and quests will often
> require you to find them. These maps will help you get started, but they are no substitute for exploring and
> making your own maps.
> 
> These maps were created with [Trizbort](trizbort.com).
> <cite>&mdash;Ayre</cite>

## Chaomonous (Underground)
Underground is a large realm featuring dark forests, a desert, castles, and lots of underground caverns.
To find Contar (every newbie's best friend), you should go East through the brambles just outside the North Gates.
<div class="well"><img class="img-responsive" src="/img/chaomonous.png" alt="Map of Chaomonous"></div>

## Haven (Haven)
Haven is a city under seige from Bandits and Demons alike. Make sure you bring some water if you dare to
venture into Haven's desert.
<div class="well"><img class="img-responsive" src="/img/haven.png" alt="Map of Haven"></div>

## St Camile (Crystal)
In Crystal, you can travel to other realms or register to be a Player Killer. If you venture far outside of the town,
you will soon run into deadly Orcs, Ogres, and Drow.
<div class="well"><img class="img-responsive" src="/img/st-camile.png" alt="Map of St Camile"></div>

## New Callis (Callis)
It's hard to miss the giant volcano in Callis. New Callis is built on top of the ruins of the old Callis, which was
destroyed when the volcano erupted. Be wary, for some of the deadliest dragons on GateWay also make their home in the
realm of Callis.
<div class="well"><img class="img-responsive" src="/img/new-callis.png" alt="Map of New Callis"></div>

## Heian-Kyo (Orient)
Home of monks and ninjas alike, the Orient is a fun place to visit. The equipment you can find in the Orient
is unlike anything else you will find in gateway.
<div class="well"><img class="img-responsive" src="/img/heian-kyo.png" alt="Map of Heian-Kyo"></div>

## Athens (Shadow)
Come test your mettle against the heros of the Greek wars, explore the minotaur's labyrinth in Crete, or become
dinner for a Cyclops. Mario's cloaks are the stuff of legend, and every adventurer will want to try some of HutMart's products.
<div class="well"><img class="img-responsive" src="/img/athens.png" alt="Map of Athens"></div>
